//
//  ViewController.m
//  WidgetView
//
//  Created by Spire Jankulovski on 9/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Create the data model
    //Check if UserDefaults has value for "autocompleteCities"
    //if not put default location to _pageTitles and save it to UserDefaults
    //else get the value
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"autocompleteCities"] == nil || [[defaults objectForKey:@"autocompleteCities"] count]==0) {
        _pageTitles = [NSMutableArray arrayWithObjects:@"Current location",nil];
        [self updateCitiesArray];
        
    }else{
        _pageTitles = [NSMutableArray arrayWithArray:[defaults objectForKey:@"autocompleteCities"]];
        
    }
    //set available widgets
    _widgets = [NSMutableArray arrayWithObjects:@"City", @"Current", @"Daily", @"Prediction", nil];
    
    //Initialise the delegate class
    dataSourceObject = [[DataSurceObject alloc] init];
    //Setting delegate
    [dataSourceObject setDelegate:self];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
}
/*
 *Gets called from DetailViewController to update the
 *number of pages (cities) and show the one whos index
 *was passed as argument
 */
-(void)updatePages:(NSMutableArray *)pagesSource atIndex:(int)index{
    _pageTitles = [NSMutableArray arrayWithArray:pagesSource];
    PageContentViewController *startingViewController = [self viewControllerAtIndex:index];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
    
}
/*
 *Gets called from PageContentViewController to update the
 *arangement of widgets
 */

-(void)updateDataSource:(NSMutableArray *)dataSource{
    _widgets = dataSource;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//Creating Page Content at index
- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.pageTitles = _pageTitles;
    pageContentViewController.widgets = _widgets;
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    pageContentViewController.dataSourceObject = dataSourceObject;
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers{
    
    
    
}
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

/*
 *update autocompleteCities value
 */
-(void)updateCitiesArray{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_pageTitles forKey:@"autocompleteCities"];
    [defaults synchronize];
}

@end