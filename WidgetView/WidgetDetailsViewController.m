//
//  WidgetDetailsViewController.m
//  PageViewDemo
//
//  Created by Spire Jankulovski on 9/6/15.
//
//

#import "WidgetDetailsViewController.h"

@interface WidgetDetailsViewController ()

@end

@implementation WidgetDetailsViewController
@synthesize cityName, imageName, currentTemperature, lastUpdated;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Receives the city name and last updated value
    //from parent controller
    NSString *combinedCityName = [NSString stringWithFormat:@"%@\n%@",cityName,lastUpdated];
    //Set label from provided value
    self.cityNameLabel.text =combinedCityName;
    
    //Setting combination of strings to present current temperature
    //Value for temperature is provided by the parent controller
    NSString *sign = @"°C";
    NSString *text = [NSString stringWithFormat:@"%@ %@", currentTemperature, sign];
    
    // If attributed text is supported (iOS6+)
    if ([self.temperatureLabel respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: self.temperatureLabel.textColor,
                                  NSFontAttributeName: self.temperatureLabel.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        // Big text attributes
        NSRange bigTextRange = [text rangeOfString:currentTemperature];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:40]}
                                range:bigTextRange];
        
        // Small text attributes
        NSRange smallTextRange = [text rangeOfString:sign];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:12]}
                                range:smallTextRange];
        //Set attributed text
        self.temperatureLabel.attributedText = attributedText;

    }else{
        //or set regular text
        self.temperatureLabel.text = currentTemperature;

    }
    //Set value to detailsLabel
    self.detailsLabel.text = @"Humidity 22% \nChances of rain 0%";
    //Set footerLabel to detailsLabel
    self.footerLabel.text = @"Praesent ultrices diam quis laoreet rutrum. Phasellus gravida, urna vitae ullamcorper commodo";
    //set image with image name provided from parent controller
    [self.weatherImageView setImage:[UIImage imageNamed:imageName]];
}
//Back button trigers dismiss
-(IBAction)dismissModalController:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
