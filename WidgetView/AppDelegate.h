//
//  AppDelegate.h
//  WidgetView
//
//  Created by Spire Jankulovski on 9/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

