//
//  DailyWeatherTableViewCell.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "DailyWeatherTableViewCell.h"
#import "OutlinedLabel.h"

@implementation DailyWeatherTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    CGSize size = self.contentView.frame.size;
    //Get current hour
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"HH"]; // Date formater
    NSString *date =[NSString stringWithFormat:@"%@", [dateformate stringFromDate:[NSDate date]]]; // Convert date to string
    int x = [date intValue];

    //Create array of numeric representation of the next 12 hours
    NSMutableArray *hoursArray = [NSMutableArray array];
    for (int i = x; i<(x+12); i++) {
        if (i>23) {
            int index = i-24;
            [hoursArray addObject:[NSString stringWithFormat:@"%d",index]];
        }else{
            [hoursArray addObject:[NSString stringWithFormat:@"%d",i]];
        }
    }

    //Create a scroll view
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, size.width, 100)];
    for (int i = 0; i <[hoursArray count]; i++)
    {
        //Add subviews to scroll view
        [self.scrollView addSubview:[self createViewForHour:[hoursArray objectAtIndex:i] atIndex:i]];
    }
    self.scrollView.contentSize = CGSizeMake(40 *[hoursArray count], 100);
    [self.contentView addSubview:self.scrollView];

}
//Creates a view of 3 elements
//Hour, image and temperature
-(UIView *)createViewForHour:(NSString *)hour atIndex:(int)index {
    //Types of images
    //are used in order, in real world will be used
    //depending on received data
    NSMutableArray *weatherImages = [NSMutableArray arrayWithObjects:@"sun.png", @"sunCloud.png", @"cloud.png", @"rain.png", @"snow.png", @"tunder.png", nil];

    CGFloat width = index * 40;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(width, 0, 40, 100)];
    UILabel *hourLabel = [[OutlinedLabel alloc] initWithFrame:CGRectMake(0, 10, 40, 30)];
    // Configure hourLabel Label
    [hourLabel setFont:[UIFont systemFontOfSize:8]];
    [hourLabel setTextAlignment:NSTextAlignmentCenter];
    [hourLabel setTextColor:[UIColor whiteColor]];
    [hourLabel setText:hour];
    [view addSubview:hourLabel];
    
    UIImageView *weatherImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, hourLabel.frame.size.height + hourLabel.frame.origin.y, 30, 26)];
    if (index>[weatherImages count]-1) {
        [weatherImageView setImage:[UIImage imageNamed:[weatherImages objectAtIndex:index - [weatherImages count]]]];

    }else{
        [weatherImageView setImage:[UIImage imageNamed:[weatherImages objectAtIndex:index]]];
    }
    [view addSubview:weatherImageView];

     self.degreesLabel = [[OutlinedLabel alloc] initWithFrame:CGRectMake(0, weatherImageView.frame.size.height + weatherImageView.frame.origin.y, 40, 30)];
    // Configure degreesLabel Label
    [self.degreesLabel setFont:[UIFont systemFontOfSize:8]];
    [self.degreesLabel setTextAlignment:NSTextAlignmentCenter];
    [self.degreesLabel setTextColor:[UIColor whiteColor]];
    [self.degreesLabel setText:@"25°C"];
    [view addSubview:self.degreesLabel];

    
    view.backgroundColor = [UIColor clearColor];
    return view;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
