//
//  WeatherPredictionTableViewCell.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "WeatherPredictionTableViewCell.h"
#import "OutlinedLabel.h"

@implementation WeatherPredictionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    CGSize size = self.contentView.frame.size;
  
    self.widgetTextLabel = [[OutlinedLabel alloc] initWithFrame:CGRectMake(10, 0, size.width - 100, size.height - 32)];
    
    // Configure cityNameLabel Label
    self.widgetTextLabel.numberOfLines = 0;
    [self.widgetTextLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
    [self.widgetTextLabel setTextAlignment:NSTextAlignmentCenter];
    [self.widgetTextLabel setTextColor:[UIColor whiteColor]];
    [self.widgetTextLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    
    // Add cityNameLabel Label to Content View
    [self.contentView addSubview:self.widgetTextLabel];
    self.widgetAnimationImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.widgetTextLabel.frame.origin.x+self.widgetTextLabel.frame.size.width, 20, 80, 80)];

    [self.contentView addSubview:self.widgetAnimationImageView];


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
