//
//  PageContentViewController.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "PageContentViewController.h"
#import "CityTableViewCell.h"
#import "CurrentTempTableViewCell.h"
#import "DailyWeatherTableViewCell.h"
#import "WeatherPredictionTableViewCell.h"
#import "AnimationImageView.h"

@interface PageContentViewController (){
    AnimationImageView *animationImagesArray ;
}

@end

@implementation PageContentViewController

NSString* const cityTableViewCell = @"CityCellIdentifier";
NSString* const currentTempTableViewCell = @"CurrentTempCellIdentifier";
NSString* const dailyWeatherTableViewCell = @"DailyWeatherCellIdentifier";
NSString* const weatherPerdictionTableViewCell = @"WeatherPredictionCellIdentifier";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.titleLabel.text = self.titleText;
    //List of images that will be used
    weatherImages = @[@"sun.png", @"sunCloud.png", @"cloud.png", @"rain.png",
                            @"snow.png", @"tunder.png"];
    

    //Gesture recogniser for rearanging widgets
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self action:@selector(longPressGestureRecognized:)];
    [self.tableView addGestureRecognizer:longPress];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, screenWidth, screenHeight)];
    //Initialization of AnimationImageView
    animationImagesArray = [[AnimationImageView alloc]init];
    NSMutableArray *imageNames = [NSMutableArray array];
    NSArray *animationsArray = [NSArray arrayWithObjects:@"sun", @"snow", @"clouds", @"rain", @"wind", @"tunder", nil];
    
    //gets array of image names to be used in animation for background view
    imageNames = [animationImagesArray arrayFromIndex:self.pageIndex forAnimationArray:animationsArray];

    //Creating array of images with the array of image names
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i < imageNames.count; i++) {
        [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
    }
    //setting images array for animating
    self.backgroundImageView.animationImages = images;
    
    self.backgroundImageView.animationDuration = 3.0;
    [self.backgroundImageView startAnimating];

}
- (void)longPressGestureRecognized:(id)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
    
    static UIView       *snapshot = nil;        // A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; // Initial index path, where gesture begins.
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                
                // Take a snapshot of the selected row using helper method.
                snapshot = [self customSnapshotFromView:cell];
                
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [self.tableView addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    cell.alpha = 0.0;
                    
                } completion:^(BOOL finished) {
                    
                    cell.hidden = YES;
                    
                }];
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                
                //change object loccation in array
                [_widgets exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                // ... move the rows.
                [self.tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                
                // ... and update source so it is in sync with UI changes.
                sourceIndexPath = indexPath;
                
                // ... update data source.
                [self.dataSourceObject setUpdatedArray:_widgets];

            }
            break;
        }
            
        default: {
            // Clean up.
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:sourceIndexPath];
            cell.hidden = NO;
            cell.alpha = 0.0;
            
            [UIView animateWithDuration:0.25 animations:^{
                
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                cell.alpha = 1.0;
                
            } completion:^(BOOL finished) {
                
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
                
            }];
            
            break;
        }
    }
}
- (UIView *)customSnapshotFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_widgets count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    float rowSize = self.view.frame.size.height/[_widgets count];
    return rowSize;
}
//Custom cell for first widget
- (UITableViewCell *)tableView:(UITableView *)tableView cityCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityTableViewCell];
    if (cell == nil) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"CityTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    cell.cityNameLabel.text = _titleText;
    //Get current date to set as a latest updated time
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"hh:mm a"]; // Date formater
    NSString *date =[NSString stringWithFormat:@"Laatste update %@", [dateformate stringFromDate:[NSDate date]]]; // Convert date to string
    cell.updateTimeLabel.text = date;

    [cell.addCityButton addTarget:self action:@selector(addOrRemoveCities) forControlEvents:UIControlEventTouchUpInside];
    
    //Setting transparent view as a cell background view
    //so it will be transparent when selected
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor clearColor];
    cell.backgroundView = backView;
    cell.selectedBackgroundView = backView;
    
    return cell;
    
}
//Custom cell for second widget
- (UITableViewCell *)tableView:(UITableView *)tableView currentTempCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CurrentTempTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityTableViewCell];
    if (cell == nil) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"CurrentTempTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    NSString *degrees = @"28";
    NSString *sign = @"°C";
    
    NSString *text = [NSString stringWithFormat:@"%@ %@", degrees, sign];
    
    // If attributed text is supported (iOS6+)
    //Create attributed text
    if ([cell.temperatureLabel respondsToSelector:@selector(setAttributedText:)]) {
        
        // Define general attributes for the entire text
        NSDictionary *attribs = @{ NSForegroundColorAttributeName: cell.temperatureLabel.textColor, NSFontAttributeName: cell.temperatureLabel.font};
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
        
        // Big text attributes
        NSRange bigTextRange = [text rangeOfString:degrees];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:40]}
                                range:bigTextRange];
        
        // Small text attributes
        NSRange smallTextRange = [text rangeOfString:sign];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:12]}
                                range:smallTextRange];
        
        //Setting weather image from weatherImages array
        //depending on the index of the page that we are at
        //In real life scenarion this will be according to the data
        //for the city that we are at
        int i = 0;
        for(NSString *city in _pageTitles){
            if ([city isEqualToString:_titleText]) {
                
                if ([weatherImages count]-1<i) {
                    long counter = i-([weatherImages count]);
                    while (counter>[weatherImages count]-1) {
                        counter = counter-([weatherImages count]);
                    }
                    [cell.weatherImageView setImage:[UIImage imageNamed:[weatherImages objectAtIndex:counter]]];
                    
                }else{
                    [cell.weatherImageView setImage:[UIImage imageNamed:[weatherImages objectAtIndex:i]]];
                }
            }
            i++;
        }
        
        cell.temperatureLabel.attributedText = attributedText;

    }
    
    cell.simpleTextLabel.text = @"Voelt als";
    return cell;
    
}
//Custom cell for third widget
- (UITableViewCell *)tableView:(UITableView *)tableView dailyWeatherCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DailyWeatherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityTableViewCell];
    if (cell == nil) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"DailyWeatherTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    

    return cell;
    
}
//Custom cell for fourth widget
- (UITableViewCell *)tableView:(UITableView *)tableView weatherPredictionCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WeatherPredictionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityTableViewCell];
    if (cell == nil) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"WeatherPredictionTableViewCell" owner:self options:nil];
        cell = [nibArray objectAtIndex:0];
    }
    
    [cell.widgetTextLabel setText:@"Lorem ipsum dolor sit amet, consectetur adipiscing elit."];
    //It was intended to have animation on every view
    //Changed to showing animation for a particular weather condition
    NSMutableArray *imageNames = [NSMutableArray array];
    NSArray *animationsArray = [NSArray arrayWithObjects: @"widgetRain", @"widgetSun", @"widgetTunder", @"widgetRain", @"widgetRain", @"widgetTunder", nil];

    //gets array of image names to be used in animation for background view
   imageNames = [animationImagesArray arrayFromIndex:self.pageIndex forAnimationArray:animationsArray];
    /*
     NSMutableArray *images = [[NSMutableArray alloc] init];
     for (int i = 0; i < imageNames.count; i++) {
     [images addObject:[UIImage imageNamed:[imageNames objectAtIndex:i]]];
     }
     cell.widgetAnimationImageView.animationImages = images;
     
     cell.widgetAnimationImageView.animationDuration = 2.0;
     [cell.widgetAnimationImageView startAnimating];
     */
    //If should use rain in widget, show image
    //and transform it to 1.3 of normal size for 6 times
    NSString *word = @"Rain";
    if ([[imageNames objectAtIndex:0] rangeOfString:word].location != NSNotFound) {
        [cell.widgetAnimationImageView setImage:[UIImage imageNamed:[imageNames objectAtIndex:0]]];

    [UIView animateWithDuration:1.5
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:(void (^)(void)) ^{
                         [UIView setAnimationRepeatCount:6];
                         cell.widgetAnimationImageView.transform=CGAffineTransformMakeScale(1.3, 1.3);
                     }
                     completion:^(BOOL finished){
                         cell.widgetAnimationImageView.transform=CGAffineTransformIdentity;
                     }];

    }
    
    return cell;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;

    //Depending on arangment of objects in _widgets array
    //the cells get aranged in the tableView
    if ([[_widgets objectAtIndex:[indexPath row]] isEqualToString:@"City"]) {
        cell = [self tableView:tableView cityCellForRowAtIndexPath:indexPath];
    } else if ([[_widgets objectAtIndex:[indexPath row]] isEqualToString:@"Current"]) {
        cell = [self tableView:tableView currentTempCellForRowAtIndexPath:indexPath];
    } else if ([[_widgets objectAtIndex:[indexPath row]] isEqualToString:@"Daily"]) {
        cell = [self tableView:tableView dailyWeatherCellForRowAtIndexPath:indexPath];
    } else {
        cell = [self tableView:tableView weatherPredictionCellForRowAtIndexPath:indexPath];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    cell.backgroundColor = [UIColor clearColor];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Cell selection only enabled for first widget on startup of the application
    //First member of _widgets array on startup
    //First member of _widgets array in ViewController
    if ([[_widgets objectAtIndex:[indexPath row]]isEqualToString:@"City"]) {
        [tableView beginUpdates];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        NSString *segueString = [NSString stringWithFormat:@"widgetDetails"];
        
        //Perform a segue.
        [self performSegueWithIdentifier:segueString sender:self];
        
        [tableView endUpdates];
    }
    
}
-(void)addOrRemoveCities{
    NSString *segueString = [NSString stringWithFormat:@"detailedView"];
    //Perform a segue.
    [self performSegueWithIdentifier:segueString sender:self];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"detailedView"])
    {
        //details provided to child view
        //the provided object is the delegate object
        DetailViewController *detailVC = segue.destinationViewController;
        detailVC.dataSourceObject = self.dataSourceObject;
    }else if ([[segue identifier] isEqualToString:@"widgetDetails"])
    {
        //details provided to child view
        WidgetDetailsViewController *detailVC = segue.destinationViewController;
        detailVC.cityName = _titleText;
        detailVC.imageName = [weatherImages objectAtIndex:_pageIndex];
        detailVC.currentTemperature = @"28";
        detailVC.lastUpdated = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
   
    }

}
-(void)reloadTableView{
    
    [self.tableView reloadData];
}
@end
