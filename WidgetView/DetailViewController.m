//
//  DetailViewController.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize cityField = cityField;
@synthesize pastCities;
@synthesize autocompleteCities;
@synthesize cityTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Checking if autocompleteCities has value
    //if not add default location and
    //update autocompleteCities
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"autocompleteCities"] == nil || [[defaults objectForKey:@"autocompleteCities"] count]==0) {
        pastCities = [NSMutableArray arrayWithObjects:@"Current location",nil];
        [self updateCitiesArray];
        
    }else{
        pastCities = [NSMutableArray arrayWithArray:[defaults objectForKey:@"autocompleteCities"]];
        
    }
    //give value to autocomplete array
    self.autocompleteCities = [NSMutableArray arrayWithArray:self.pastCities];
    //create table view
    cityTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, 320,  self.view.frame.size.height -  cityField.frame.origin.y - cityField.frame.size.height) style:UITableViewStylePlain];
    cityTableView.delegate = self;
    cityTableView.dataSource = self;
    cityTableView.scrollEnabled = YES;
    cityTableView.allowsMultipleSelectionDuringEditing = NO;
    [self.view addSubview:cityTableView];


}
- (IBAction)goPressed:(id)sender{
    
    // Clean up UI
    [cityField resignFirstResponder];
    [cityTableView setFrame:CGRectMake(cityTableView.frame.origin.x, cityTableView.frame.origin.y, cityTableView.frame.size.width, self.view.frame.size.height -  cityField.frame.origin.y - cityField.frame.size.height)];

    //check if field has value
    if ([cityField.text length]>1) {
        //update options array if it is a new value
        if (![pastCities containsObject:cityField.text]) {
            [pastCities addObject:cityField.text];
        }
        //get index of selected value
        int index = 0;
        for (int i = 0;i<[pastCities count];i++) {
            NSString *someString  = [NSString stringWithFormat:@"%@",[pastCities objectAtIndex:i]];
            if ([someString isEqualToString:cityField.text]) {
                index = i;
            }
        }
        //update autocompleteCities value
        [self updateCitiesArray];
        //call update to ViewController
        //set data array (cities array) to new value
        //sets index of page to open
        [self.dataSourceObject setCurrentPages:pastCities atIndex:index];
        [self dismissViewControllerAnimated:YES completion:nil];
        

    }else{
        [cityTableView reloadData];

    }
}
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [autocompleteCities removeAllObjects];
    if (substring.length>0) {
        for(NSString *curString in pastCities) {
            NSRange substringRange = [curString rangeOfString:substring  options:NSCaseInsensitiveSearch];
            if (substringRange.location == 0) {
                [autocompleteCities addObject:curString];
            }
        }

    }else{
        for(NSString *curString in pastCities) {
            [autocompleteCities addObject:curString];
        }
    }
    [cityTableView reloadData];
}

#pragma mark UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchAutocompleteEntriesWithSubstring:substring];
    
    return YES;
}

#pragma mark UITableViewDataSource methods
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Current Location can not be deleted.
    if ([indexPath row]==0) {
        return NO;
    }else
        return YES;

}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //removing object from cities array on screen
        [pastCities removeObjectAtIndex:[indexPath row]];
        //remove onbject from array that is used to update
        //autocompleteCities object from UserDefault
        [autocompleteCities removeObjectAtIndex:[indexPath row]];
        [cityTableView reloadData];
        //Update UserDefaults
        [self updateCitiesArray];
        //set data array (cities array) to new value
        //sets index of page to open
        [self.dataSourceObject setCurrentPages:pastCities atIndex:0];

    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {

    return autocompleteCities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                 initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
        cell.textLabel.text = [autocompleteCities objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cityField isFirstResponder]) {
        //If user types and selects value from autocomplete results
        UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        //fill text field with the autocomplete value
        cityField.text = selectedCell.textLabel.text;
        [self goPressed:tableView];
    }else {
        //If row is selected when user is not writing
        //perform selection.
        //update UserDefaults
        [self updateCitiesArray];
        //set new value to ViewController (cities) array and provide
        //index of controller to show
        [self.dataSourceObject setCurrentPages:pastCities atIndex:[indexPath row]];
        //dismiss the modal view
        [self dismissViewControllerAnimated:YES completion:nil];

    }
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [cityTableView setFrame:CGRectMake(0, 100, 320, 120)];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)dismissModalController{
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
 *Method that updates the value of autocompleteCities object
 *adding to it the value of the array that contains the cities
 */
-(void)updateCitiesArray{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:pastCities forKey:@"autocompleteCities"];
    [defaults synchronize];
}
- (IBAction)dismissKeyboard:(id)sender {
    [cityTableView setFrame:CGRectMake(cityTableView.frame.origin.x, cityTableView.frame.origin.y, cityTableView.frame.size.width, self.view.frame.size.height -  cityField.frame.origin.y - cityField.frame.size.height)];
    [cityField resignFirstResponder];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [cityTableView setFrame:CGRectMake(cityTableView.frame.origin.x, cityTableView.frame.origin.y, cityTableView.frame.size.width, self.view.frame.size.height -  cityField.frame.origin.y - cityField.frame.size.height)];
    [textField resignFirstResponder];
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
