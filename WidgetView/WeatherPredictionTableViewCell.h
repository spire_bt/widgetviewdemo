//
//  WeatherPredictionTableViewCell.h
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import <UIKit/UIKit.h>

@interface WeatherPredictionTableViewCell : UITableViewCell
@property (nonatomic, strong)UILabel *widgetTextLabel;
@property (nonatomic, strong)UIImageView *widgetAnimationImageView;
@end
