//
//  DailyWeatherTableViewCell.h
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import <UIKit/UIKit.h>

@interface DailyWeatherTableViewCell : UITableViewCell
@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UILabel *degreesLabel;
-(UIView *)createViewForHour:(NSString *)hour atIndex:(int)index;
@end
