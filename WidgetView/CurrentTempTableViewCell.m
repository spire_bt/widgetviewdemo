//
//  CurrentTempTableViewCell.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "CurrentTempTableViewCell.h"
#import "OutlinedLabel.h"

@implementation CurrentTempTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    CGSize size = self.contentView.frame.size;

    self.simpleTextLabel = [[OutlinedLabel alloc] initWithFrame:CGRectMake(10, 10, size.width - 100, 30)];
    // Configure simpleTextLabel Label
    [self.simpleTextLabel setFont:[UIFont systemFontOfSize:12]];
    [self.simpleTextLabel setTextAlignment:NSTextAlignmentCenter];
    [self.simpleTextLabel setTextColor:[UIColor whiteColor]];

    // Add simpleTextLabel Label to Content View
    [self.contentView addSubview:self.simpleTextLabel];
    
    self.temperatureLabel = [[OutlinedLabel alloc] initWithFrame:CGRectMake(10, self.simpleTextLabel.frame.size.height + self.simpleTextLabel.frame.origin.y + 10, size.width - 100, 40)];
    // Configure temperatureLabel Label
    //[self.temperatureLabel setFont:[UIFont systemFontOfSize:8.0]];
    [self.temperatureLabel setTextAlignment:NSTextAlignmentCenter];
    [self.temperatureLabel setTextColor:[UIColor whiteColor]];
    
    // Add temperatureLabel Label to Content View
    [self.contentView addSubview:self.temperatureLabel];

    self.weatherImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.simpleTextLabel.frame.origin.x+self.simpleTextLabel.frame.size.width+20, 20, 70, 65)];
    [self.contentView addSubview:self.weatherImageView];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
