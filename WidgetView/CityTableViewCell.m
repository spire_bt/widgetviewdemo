//
//  CityTableViewCell.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "CityTableViewCell.h"
#import "OutlinedLabel.h"

@implementation CityTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleDefault;
    [self.contentView setBackgroundColor:[UIColor clearColor]];
    CGSize size = self.contentView.frame.size;

    // Initialize cityNameLabel Label
    self.cityNameLabel = [[OutlinedLabel alloc] initWithFrame:CGRectMake(50, 5, size.width - 100, size.height - 16.0)];
    
    // Configure cityNameLabel Label
    [self.cityNameLabel setFont:[UIFont boldSystemFontOfSize:24.0]];
    [self.cityNameLabel setTextAlignment:NSTextAlignmentCenter];
    [self.cityNameLabel setTextColor:[UIColor whiteColor]];
    [self.cityNameLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    
   
    // Add cityNameLabel Label to Content View
    [self.contentView addSubview:self.cityNameLabel];
    
    // Initialize updateTimeLabel Label
    self.updateTimeLabel = [[OutlinedLabel alloc] initWithFrame:CGRectMake(50, self.cityNameLabel.frame.size.height + self.cityNameLabel.frame.origin.y + 10, size.width - 100, size.height - 16.0)];
    
    // Configure updateTimeLabel Label
    [self.updateTimeLabel setFont:[UIFont boldSystemFontOfSize:18.0]];
    [self.updateTimeLabel setTextAlignment:NSTextAlignmentCenter];
    [self.updateTimeLabel setTextColor:[UIColor whiteColor]];
    [self.updateTimeLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    
    // Add updateTimeLabel Label to Content View
    [self.contentView addSubview:self.updateTimeLabel];
    
    self.addCityButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    [self.addCityButton setFrame:CGRectMake(self.cityNameLabel.frame.size.width + self.cityNameLabel.frame.origin.x + 10, self.updateTimeLabel.frame.origin.y, 30, 30)];
    self.addCityButton.tintColor = [UIColor whiteColor];
    [self.addCityButton layer].shadowColor = [[UIColor blackColor] CGColor];
    [self.addCityButton layer].shadowOffset = CGSizeMake(0.0f, 1.0f);
    [self.addCityButton layer].shadowOpacity = 1.0f;
    [self.addCityButton layer].shadowRadius = 1.0f;

    [self.contentView addSubview:self.addCityButton];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
