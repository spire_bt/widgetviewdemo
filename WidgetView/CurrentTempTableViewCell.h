//
//  CurrentTempTableViewCell.h
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import <UIKit/UIKit.h>

@interface CurrentTempTableViewCell : UITableViewCell
@property(nonatomic, strong) UILabel *simpleTextLabel;
@property(nonatomic, strong) UILabel *temperatureLabel;
@property(nonatomic, strong) UIImageView *weatherImageView;

@end
