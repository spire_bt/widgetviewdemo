//
//  AnimationImageView.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "AnimationImageView.h"

@implementation AnimationImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)init
{
    if (self = [super init]) {
    }
    return self;
}
/*
 *It receives array with types of animations
 *the type is actually name of collection of 18 images that
 *have numbers e.x. snow1.png, snow2.png...
 *It receives the index of the view and based on the index
 *calculates what image names collection to return in array
 */
-(NSMutableArray *)arrayFromIndex:(NSUInteger)index forAnimationArray:(NSArray *)animationsArray{
    
    NSString *animationTypeString = [NSString string];
   
    if ([animationsArray count]-1<index) {
        long counter = index-([animationsArray count]);
        while (counter>[animationsArray count]-1) {
            counter = counter-([animationsArray count]);
        }
        animationTypeString = [animationsArray objectAtIndex:counter];
        
    }else{
        animationTypeString = [animationsArray objectAtIndex:index];
        
    }
   NSMutableArray *imageNamesArray = [NSMutableArray array];
    for (int i = 1; i<19; i++) {
        [imageNamesArray addObject:[NSString stringWithFormat:@"%@%d.png",animationTypeString,i]];
    }
    
    return imageNamesArray;
}

@end
