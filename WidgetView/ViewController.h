//
//  ViewController.h
//  WidgetView
//
//  Created by Spire Jankulovski on 9/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"
#import "DataSurceObject.h"

@interface ViewController : UIViewController <UIPageViewControllerDataSource, DataSurceObjectDelegate>{
    DataSurceObject *dataSourceObject;
}

@property (strong, nonatomic) UIPageViewController *pageViewController;
//pageTitles is the array that presents the number of cities and there names
@property (strong, nonatomic) NSMutableArray *pageTitles;
//widgets is the array that presents the number of widgets and there names
@property (strong, nonatomic) NSMutableArray *widgets;

@end


