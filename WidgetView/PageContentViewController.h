//
//  PageContentViewController.h
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import <UIKit/UIKit.h>
#import "DataSurceObject.h"
#import "DetailViewController.h"
#import "WidgetDetailsViewController.h"

@interface PageContentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    UIImageView *animationImageView;
    UIImageView *slowAnimationImageView;
    NSArray *weatherImages;

}
//Object provided from parent controller
//parent controller delegate, to be used in child controllers
@property (nonatomic, strong) DataSurceObject *dataSourceObject;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property (nonatomic, strong) NSMutableArray * pageTitles;
@property (nonatomic, strong) NSMutableArray * widgets;
- (UIView *)customSnapshotFromView:(UIView *)inputView;
@end
