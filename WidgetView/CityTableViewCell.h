//
//  CityTableViewCell.h
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import <UIKit/UIKit.h>

@interface CityTableViewCell : UITableViewCell
@property(nonatomic, strong)UILabel *cityNameLabel;
@property(nonatomic, strong)UILabel *updateTimeLabel;
@property(nonatomic, strong)UIButton *addCityButton;
@end
