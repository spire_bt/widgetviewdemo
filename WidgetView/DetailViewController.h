//
//  DetailViewController.h
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import <UIKit/UIKit.h>
#import "DataSurceObject.h"

@interface DetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    IBOutlet UITextField *cityField;
    NSMutableArray *pastCities;
    NSMutableArray *autocompleteCities;
    UITableView *cityTableView;

}
//Object provided from parent controller
//parent controller delegate, to be used in child controllers
//used for updating view when modal view is dismissed
@property (nonatomic, strong) DataSurceObject *dataSourceObject;

@property (nonatomic, strong) UITextField *cityField;
@property (nonatomic, strong) NSMutableArray *pastCities;
@property (nonatomic, strong) NSMutableArray *autocompleteCities;
@property (nonatomic, strong) UITableView *cityTableView;

- (IBAction)goPressed:(id)sender;
- (IBAction)dismissKeyboard:(id)sender ;
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring;

-(IBAction)dismissModalController;
@end
