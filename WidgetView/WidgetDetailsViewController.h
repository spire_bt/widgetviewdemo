//
//  WidgetDetailsViewController.h
//  PageViewDemo
//
//  Created by Spire Jankulovski on 9/6/15.
//
//

#import <UIKit/UIKit.h>

@interface WidgetDetailsViewController : UIViewController

@property(nonatomic, weak) IBOutlet UILabel *cityNameLabel;
@property(nonatomic, weak) IBOutlet UILabel *temperatureLabel;
@property(nonatomic, weak) IBOutlet UILabel *detailsLabel;
@property(nonatomic, weak) IBOutlet UILabel *footerLabel;
@property(nonatomic, weak) IBOutlet UIImageView *weatherImageView;
@property(nonatomic, weak) IBOutlet UIButton *backButton;
@property(nonatomic, strong) NSString *cityName;
@property(nonatomic, strong) NSString *imageName;
@property(nonatomic, strong) NSString *currentTemperature;
@property(nonatomic, strong) NSString *lastUpdated;
-(IBAction)dismissModalController:(id)sender;
@end
