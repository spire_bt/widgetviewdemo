//
//  DataSurceObject.m
//  WidgetView
//
//  Created by Spire Jankulovski on 9/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "DataSurceObject.h"

@implementation DataSurceObject
@synthesize delegate;

- (id)init
{
    if (self = [super init]) {
        
    }
    return self;
}
/*
 *Updating widgets arangement
 */
-(void)setUpdatedArray:(NSMutableArray*)array{
    NSMutableArray *updatedArray =[NSMutableArray array];
    updatedArray = array;
    
    [[self delegate] updateDataSource:updatedArray];


}
/*
 *Update Pages (Cities)
 */
-(void)setCurrentPages:(NSMutableArray *)array atIndex:(int)index {
    NSMutableArray *updatedArray =[NSMutableArray array];
    updatedArray = array;
    
    [[self delegate]updatePages:updatedArray atIndex:index];
    
}

@end
