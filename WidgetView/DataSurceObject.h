//
//  DataSurceObject.h
//  WidgetView
//
//  Created by Spire Jankulovski on 9/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol DataSurceObjectDelegate <NSObject>
-(void)updateDataSource:(NSMutableArray *)dataSource;
-(void)updatePages:(NSMutableArray *)pagesSource atIndex:(int)index;

@end

@interface DataSurceObject : NSObject{
    __weak id <DataSurceObjectDelegate> delegate;

}
@property (nonatomic, weak)  id delegate;
-(void)setUpdatedArray:(NSMutableArray*)array;
-(void)setCurrentPages:(NSMutableArray*)array atIndex:(int)index;
@end
