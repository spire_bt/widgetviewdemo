//
//  OutlinedLabel.m
//
//  Created by Spire Jankulovski on 9/6/15.
//

#import "OutlinedLabel.h"

@implementation OutlinedLabel


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//Custom Label that ha shadow on text
- (void)drawRect:(CGRect)rect {

    [self setTextAlignment:NSTextAlignmentCenter];
    UIColor *textColor = self.textColor;
    [self setTextColor:textColor];
    [self setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    
    [self layer].shadowColor = [[UIColor blackColor] CGColor];
    [self layer].shadowOffset = CGSizeMake(0.0f, 1.0f);
    [self layer].shadowOpacity = 1.0f;
    [self layer].shadowRadius = 1.0f;
    [super drawTextInRect:rect];


}


@end
